# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
# Copyright (C) 2019  David Foster 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#  
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import pickle
import music21
import numpy as np
from . utils import *
from . config import *
from . network import make_network


class Composer():
    def __init__(self):
        self._dirs = directories()
        self._distincts, self._lookups = self._load_tables(self._dirs["store"])
        self._note_to_int = self._lookups[0]
        self._int_to_note = self._lookups[1]
        self._duration_to_int = self._lookups[2]
        self._int_to_duration = self._lookups[3]
        self._note_names = self._distincts[0]
        self._n_notes = self._distincts[1]
        self._duration_names = self._distincts[2]
        self._n_durations = self._distincts[3]
        self._model, self._att_model = self._rebuild_model(self._distincts, self._dirs["weights"])

    @property
    def model(self):
        return self._model

    @property
    def att_model(self):
        return self._att_model

    @property
    def vocabulary(self):
        return self._distincts

    @property
    def tables(self):
        return self._lookups

    def _load_tables(self, store_dir):
        with open(os.path.join(store_dir, "distincts"), "rb") as filepath:
            distincts = pickle.load(filepath)
            #note_names, n_notes, duration_names, n_durations = distincts

        with open(os.path.join(store_dir, "lookups"), "rb") as filepath:
            lookups = pickle.load(filepath)
            #note_to_int, int_to_note, duration_to_int, int_to_duration = lookups

        return distincts, lookups

    def _rebuild_model(self, distincts, weights_dir):
        n_notes = distincts[1]
        n_durations = distincts[3]
        weights_file = "weights.h5"
        model, att_model = make_network(n_notes, n_durations, EMBED_SIZE, RNN_UNITS, USE_ATTENTION)
        # load the weights
        weights = os.path.join(weights_dir,weights_file)
        model.load_weights(weights)

        return model, att_model

    def _make_predictions(self, seed, notes_temp, duration_temp, max_extra_notes, max_seq_len, seq_len):
        notes = seed[0]
        durations = seed[1]

        if seq_len is not None:
            notes = ["START"] * (seq_len - len(notes)) + notes
            durations = [0] * (seq_len - len(durations)) + durations

        sequence_length = len(notes)
        predictions = []
        notes_input_sequence = []
        durations_input_sequence = []

        distribution = []

        for n, d in zip(notes,durations):
            note_int = self._note_to_int[n]
            duration_int = self._duration_to_int[d]

            notes_input_sequence.append(note_int)
            durations_input_sequence.append(duration_int)

            predictions.append([n, d])

            if n != "START":
                midi_note = music21.note.Note(n)

                new_note = np.zeros(128)
                new_note[midi_note.pitch.midi] = 1
                distribution.append(new_note)

        att_matrix = np.zeros(shape = (max_extra_notes+sequence_length, max_extra_notes))

        for note_index in range(max_extra_notes):

            prediction_input = [
                np.array([notes_input_sequence])
                , np.array([durations_input_sequence])
               ]

            notes_prediction, durations_prediction = self._model.predict(prediction_input, verbose=0)
            if USE_ATTENTION:
                att_prediction = self._att_model.predict(prediction_input, verbose=0)[0]
                att_matrix[(note_index-len(att_prediction)+sequence_length):(note_index+sequence_length), note_index] = att_prediction

            new_note = np.zeros(128)

            for idx, n_i in enumerate(notes_prediction[0]):
                try:
                    note_name = self._int_to_note[idx]
                    midi_note = music21.note.Note(note_name)
                    new_note[midi_note.pitch.midi] = n_i

                except:
                    pass

            distribution.append(new_note)

            i1 = tsample(notes_prediction[0], notes_temp)
            i2 = tsample(durations_prediction[0], duration_temp)

            note_result = self._int_to_note[i1]
            duration_result = self._int_to_duration[i2]

            predictions.append([note_result, duration_result])

            notes_input_sequence.append(i1)
            durations_input_sequence.append(i2)

            if len(notes_input_sequence) > max_seq_len:
                notes_input_sequence = notes_input_sequence[1:]
                durations_input_sequence = durations_input_sequence[1:]

            if note_result == "START":
                break

        distribution = np.transpose(np.array(distribution))

        return predictions, distribution, att_matrix

    def compose(self, seed, notes_temp, duration_temp, max_extra_notes, max_seq_len, seq_len):
        return self._make_predictions(seed, notes_temp, duration_temp, max_extra_notes, max_seq_len, seq_len)
