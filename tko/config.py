# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#  
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
ROOT_DIR = os.path.realpath(os.path.dirname(__file__))
# directories
DATA_DIR = os.path.join(ROOT_DIR, "data")
OUT_DIR = os.path.join(ROOT_DIR, "out")
RUN_DIR = os.path.join(ROOT_DIR, "run")
# music params
SEQ_LEN = 32
TRANS = "C" # "All", None
# model params
MODE = "build" # 'load'
EMBED_SIZE = 100
RNN_UNITS = 256
USE_ATTENTION = True
NUM_EPOCHS = 1000
BATCH_SIZE = 32
VAL_SPLIT = 0.2
SHUFFLE = True
