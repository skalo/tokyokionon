# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#  
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import random
import music21
from . sonifying import sonify
from . utils import directories, stream_to_file, default_kwargs

dirs = directories()
MID_DIR = dirs["mid"]

ddict = {
         1.0 : 'quarter',
         0.5 : 'eighth',
         0: 'zero'
        }

@default_kwargs(year_span=[1876, 2021], seed=[["A4"],[1.0]], max_extra_notes=16, max_seq_len=32, seq_len=32, save_midi=True)
def select(**kwargs):
    start_year, end_year = kwargs["year_span"]
    out_dict = {}
    year_seq = list(range(start_year, end_year))
    s = kwargs["seed"]
    ss = '_'.join([s[0][0], ddict[s[1][0]]])
    mxx = kwargs["max_extra_notes"]
    mxl = kwargs["max_seq_len"]
    sql = kwargs["seq_len"]
    melodies = []
    for idx, y in enumerate(year_seq):
        # network's hyperparams: "year", "seed", "max_extra_notes", "max_seq_len", "seq_len"
        stream, _ = sonify(year=y, seed=s, max_extra_notes=mxx, max_seq_len=mxl, seq_len=sql, save=False)
        flattened_stream = music21.stream.Stream()
        for el in stream.flat.notesAndRests:
            if el.isNote or el.isRest:
                flattened_stream.append(el)
            elif el.isChord:
                flattened_stream.append(music21.note.Note(el.root(), duration=el.duration))
        melodies.append(flattened_stream)
        #melodies.append(stream)
    concatenated_stream = music21.stream.Stream()
    for melody in melodies:
        concatenated_stream.append(melody)
    if kwargs["save_midi"]:
        filename = "tokyokionon_{}-{}_{}_{}_{}_{}".format(str(start_year), str(end_year), ss, str(mxx), str(mxl), str(sql))
        stream_to_file(concatenated_stream, MID_DIR, filename, format="midi")

    return melodies, concatenated_stream
