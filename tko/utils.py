# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
# Copyright (C) 2019  David Foster 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#  
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import glob
import pickle
import music21
import requests
import functools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from keras.utils import np_utils
from bs4 import BeautifulSoup as soup
from scipy.signal import find_peaks
from scipy.spatial.distance import cosine
from sklearn.preprocessing import MinMaxScaler
from . config import DATA_DIR, OUT_DIR, RUN_DIR

CSV_DIR = os.path.join(DATA_DIR, "csv")


def make_tokyo_kion_df():
    if not os.path.exists(CSV_DIR):
        os.mkdir(CSV_DIR)
    df_path = os.path.join(CSV_DIR, "tokyo_kion_df.csv")
    url = "https://www.data.jma.go.jp/obd/stats/etrn/view/monthly_s3_en.php?block_no=47662&view=2"
    r = requests.get(url)
    s = soup(r.content.decode("utf-8"),"lxml")
    t = s.find_all("table")
    df = pd.read_html(str(t))[1]
    df = df.applymap(lambda x: x.strip().split()[0] if isinstance(x, str) else x)
    df.iloc[:, 1::] = df.iloc[:, 1::].applymap(lambda x: float(x))
    tokyo_kion_df = df.dropna()
    tokyo_kion_df.to_csv(df_path, index=False)

    return tokyo_kion_df


def load_tokyo_kion_df():
    df_path = os.path.join(CSV_DIR, "tokyo_kion_df.csv")
    if not os.path.exists(df_path):
        return None
    else:
        return pd.read_csv(df_path)


def tokyo_kion():
    try:
        tokyo_kion_df = load_tokyo_kion_df()
    except:
        tokyo_kion_df = make_tokyo_kion_df()

    fwd_diff = tokyo_kion_df.iloc[:, 2:-1].apply(lambda x: np.round(np.diff(x), 3), axis=1)

    fwd_diff_sim = [0.0]
    for i in range(1, len(fwd_diff)):
        fwd_diff_sim.append(np.round(cosine(fwd_diff[i].reshape(1,-1), fwd_diff[0].reshape(1,-1)),3))

    yearwise_fwd_diff_sim = np.asarray(fwd_diff_sim)
    annual_mean_max = tokyo_kion_df.iloc[:, -1].values

    scaler = MinMaxScaler()

    yearwise_fwd_diff_sim_scaled = np.round(scaler.fit_transform(yearwise_fwd_diff_sim.reshape(-1,1)).reshape(1,-1)[0], 3)
    annual_mean_max_scaled = np.round(scaler.fit_transform(annual_mean_max.reshape(-1,1)).reshape(1,-1)[0],3)

    return tokyo_kion_df, yearwise_fwd_diff_sim_scaled, annual_mean_max_scaled


def directories():
    directories = {}
    data_dir = DATA_DIR
    out_dir = OUT_DIR
    run_dir = RUN_DIR

    csv_dir = os.path.join(data_dir, "csv")
    xml_dir = os.path.join(data_dir, "xml")

    store_dir = os.path.join(run_dir, "store")
    weights_dir = os.path.join(run_dir, "weights")

    mid_dir = os.path.join(out_dir, "mid")
    viz_dir = os.path.join(out_dir, "viz")

    if not os.path.exists(run_dir):
        os.mkdir(run_dir)
        os.mkdir(store_dir)
        os.mkdir(weights_dir)

    if not os.path.exists(out_dir):
        os.mkdir(mid_dir)
        os.mkdir(viz_dir)

    directories["data"] = data_dir
    directories["out"] = out_dir
    directories["run"] = run_dir

    directories["csv"] = csv_dir
    directories["xml"] = xml_dir

    directories["store"] = store_dir
    directories["weights"] = weights_dir

    directories["mid"] = mid_dir
    directories["viz"] = viz_dir

    return directories


def transpose_to_c(score):
    majors = dict(
        [("A-", 4), ("A", 3), ("B-", 2), ("B", 1), ("C", 0), ("D-", -1), ("D", -2), ("E-", -3), ("E", -4), ("F", -5),
         ("G-", 6), ("G", 5)])
    minors = dict(
        [("A-", 1), ("A", 0), ("B-", -1), ("B", -2), ("C", -3), ("D-", -4), ("D", -5), ("E-", 6), ("E", 5), ("F", 4),
         ("G-", 3), ("G", 2)])
    key = score.analyze('key')  # <music21.key.Key>
    semitones = 0
    if key.mode == "major":
        semitones = majors[key.tonic.name]
    elif key.mode == "minor":
        semitones = minors[key.tonic.name]
    transposed_score = score.transpose(semitones)

    return semitones, transposed_score


def playlist(xml_dir):
    file_list = glob.glob(os.path.join(data_dir, "*.xml"))

    return file_list


def unique(elements):
    element_names = sorted(set(elements))
    n_elements = len(element_names)

    return (element_names, n_elements)


def lookup(element_names):
    element_to_int = dict((element, number) for number, element in enumerate(element_names))
    int_to_element = dict((number, element) for number, element in enumerate(element_names))

    return (element_to_int, int_to_element)


def sequences(notes, durations, lookups, distincts, seq_len =32):
    note_to_int, int_to_note, duration_to_int, int_to_duration = lookups
    note_names, n_notes, duration_names, n_durations = distincts

    notes_network_input = []
    notes_network_output = []
    durations_network_input = []
    durations_network_output = []

    for i in range(len(notes) - seq_len):
        notes_sequence_in = notes[i:i + seq_len]
        notes_sequence_out = notes[i + seq_len]
        notes_network_input.append([note_to_int[char] for char in notes_sequence_in])
        notes_network_output.append(note_to_int[notes_sequence_out])

        durations_sequence_in = durations[i:i + seq_len]
        durations_sequence_out = durations[i + seq_len]
        durations_network_input.append([duration_to_int[char] for char in durations_sequence_in])
        durations_network_output.append(duration_to_int[durations_sequence_out])

    n_patterns = len(notes_network_input)

    # reshape the input into a format compatible with LSTM layers
    notes_network_input = np.reshape(notes_network_input, (n_patterns, seq_len))
    durations_network_input = np.reshape(durations_network_input, (n_patterns, seq_len))
    network_input = [notes_network_input, durations_network_input]

    notes_network_output = np_utils.to_categorical(notes_network_output, num_classes=n_notes)
    durations_network_output = np_utils.to_categorical(durations_network_output, num_classes=n_durations)
    network_output = [notes_network_output, durations_network_output]

    return (network_input, network_output)


def tsample(predictions, temperature):
    if temperature == 0:
        sample = np.argmax(predictions)
    else:
        predictions = np.log(predictions) / temperature
        exp_predictions = np.exp(predictions)
        predictions = exp_predictions / np.sum(exp_predictions)
        sample = np.random.choice(len(predictions), p=predictions)

    return sample


def predictions_to_stream(prediction_output):
    music_stream = music21.stream.Stream()
    music_stream.append(music21.instrument.Shamisen())

    for pattern in prediction_output:
        note_pattern, duration_pattern = pattern
        # pattern is a chord
        if ('.' in note_pattern):
            notes_in_chord = note_pattern.split('.')
            chord_notes = []
            for current_note in notes_in_chord:
                new_note = music21.note.Note(current_note)
                new_note.duration = music21.duration.Duration(duration_pattern)
                new_note.storedInstrument = music21.instrument.Shamisen()
                chord_notes.append(new_note)
            new_chord = music21.chord.Chord(chord_notes)
            music_stream.append(new_chord)
        elif note_pattern == "rest":
        # pattern is a rest
            new_note = music21.note.Rest()
            new_note.duration = music21.duration.Duration(duration_pattern)
            new_note.storedInstrument = music21.instrument.Shamisen()
            music_stream.append(new_note)
        elif note_pattern != "START":
        # pattern is a note
            new_note = music21.note.Note(note_pattern)
            new_note.duration = music21.duration.Duration(duration_pattern)
            new_note.storedInstrument = music21.instrument.Shamisen()
            music_stream.append(new_note)

    music_stream = music_stream.chordify()

    return music_stream


def stream_to_file(music_stream, output_dir, outfile, format="midi"):
    assert format in ["midi", "musicxml"], "Only midi or musicxml, please"
    if format == "midi":
        extension = ".mid"
    else:
        extension = ".mxl"
    outfile_path = os.path.join(output_dir, outfile+extension)
    music_stream.write(format, fp=outfile_path)


def play_midifile(filename):
    mf = music21.midi.MidiFile()
    mf.open(filename)
    mf.read()
    s = music21.midi.translate.midiFileToStream(mf)
    mf.close()
    s.show('midi')


def save_show(outfile, show):
    if outfile != None:
        outfile_path = outfile+".png"
        plt.savefig(outfile_path)
    if show:
        plt.show()
    plt.close('all')


def plot_loss(history, component="pitch", outfile=None, show=True):
    assert component in ["pitch", "duration"]
    loss = "{}_loss".format(component)
    val_loss = "val_{}_loss".format(component)
    fig, ax = plt.subplots(figsize=(10,10))
    plt.plot(history[loss])
    plt.plot(history[val_loss])
    plt.title("{} loss".format(component))
    plt.ylabel("loss")
    plt.xlabel("epoch")
    plt.legend(["train", "test"], loc="upper left")
    save_show(outfile, show)


def plot_distribution(distro, max_extra_notes, outfile=None, show=True):
    fig, ax = plt.subplots(figsize=(10,10))
    ax.set_yticks([int(j) for j in range(35,70)])
    plt.imshow(distro[35:70,:], origin="lower", cmap="coolwarm", vmin = -0.5, vmax = 0.5, extent=[0, max_extra_notes, 35,70])
    save_show(outfile, show)


def plot_attention(att_matrix, predictions, seq_len, outfile=None, show=True):
    fig, ax = plt.subplots(figsize=(10,10))
    im = ax.imshow(att_matrix[(seq_len-2):,], cmap="coolwarm", interpolation="nearest")
    # minor ticks
    ax.set_xticks(np.arange(-.5, len(predictions)- seq_len, 1), minor=True)
    ax.set_yticks(np.arange(-.5, len(predictions)- seq_len, 1), minor=True)
    # minor ticks' gridlines
    ax.grid(which="minor", color="black", linestyle="-", linewidth=1)
    # show all ticks
    ax.set_xticks(np.arange(len(predictions) - seq_len))
    ax.set_yticks(np.arange(len(predictions)- seq_len+2))
    # label ticks with the corresponding list entries
    ax.set_xticklabels([n[0] for n in predictions[(seq_len):]])
    ax.set_yticklabels([n[0] for n in predictions[(seq_len - 2):]])
    # ax.grid(color='black', linestyle='-', linewidth=1)
    ax.xaxis.tick_top()
    plt.setp(ax.get_xticklabels(), rotation=90, ha="left", va = "center",
             rotation_mode="anchor")
    save_show(outfile, show)


def pickle_dict(dictionary, filepath):
    with open(filepath, "wb") as f:
        pickle.dump(dictionary, f)
    f.close()


def unpickle_dict(pickled_dict):
    with open(pickled_dict, "rb") as f:
        unpickled_dict = pickle.load(f, encoding='bytes')
    return unpickled_dict


def series(stream):
    return np.array([e.pitch.midi if e.isNote else 0 for e in stream.recurse().flat.notesAndRests])


def notes(stream):
    return np.array([e.pitch.midi for e in stream.recurse().flat.notes])


def notes_range(stream):
    return [np.min(notes(stream)), np.max(notes(stream))]


def durations(stream):
    return np.array([e.duration.quarterLength for e in stream.recurse().flat.notesAndRests])


def durations_range(stream):
    return [np.min(durations(stream)), np.max(durations(stream))]


def duration(stream):
    return sum(durations(stream))


def mean_note(stream):
    return np.round(np.mean(notes(stream)),3)


def mean_duration(stream):
    return np.round(np.mean(durations(stream)),3)


def peaks(series):
    # indices of maxima
    maxima, _ = find_peaks(series, height=min(series))
    # indices of minima
    # sort out possible zeros (rests)
    new_series = series.copy()
    new_series[new_series == 0] = max(new_series)
    minima, _ = find_peaks(np.multiply(new_series, -1), height=-max(new_series))
    return maxima, minima


def describe(stream):
    d = duration(stream)
    n_range = notes_range(stream)
    d_range = durations_range(stream)
    mean_n = mean_note(stream)
    mean_d = mean_duration(stream)
    maxima, minima = peaks(series(stream))
    return d, n_range, d_range, mean_n, mean_d, maxima, minima


def default_kwargs(**defaultKwargs):
    def actual_decorator(fn):
        @functools.wraps(fn)
        def g(*args, **kwargs):
            defaultKwargs.update(kwargs)
            return fn(*args, **defaultKwargs)
        return g
    return actual_decorator