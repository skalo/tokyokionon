# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
# Copyright (C) 2019  David Foster 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#   
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import pickle
import music21
import tensorflow.keras as keras
from keras.callbacks import ModelCheckpoint, EarlyStopping
from . network import make_network
from . utils import directories, transpose_to_c, playlist, unique, lookup, sequences
from . config import *


class Learner():
    def __init__(self):
        self._dirs = directories()
        self._notes, self._durations = self._extract_notes()
        self._distincts, self._lookups = self._lookup_tables()

    def _extract_notes(self):
        if MODE == "build":
            music_list = playlist(self._dirs["xml"])
            notes = []
            durations = []

            for i, file in enumerate(music_list):
                print(i+1, "Parsing %s" % file)
                original_score = music21.converter.parse(file).chordify()

                if TRANS == "C":
                    s, _ = transpose_to_c(original_score)
                    semitones = [s]
                elif TRANS == "All":
                    semitones = range(12)
                else:
                    semitones = [0]

                for semitone in semitones:

                    score = original_score.transpose(semitone)

                    notes.extend(["START"] * SEQ_LEN)
                    durations.extend([0]* SEQ_LEN)

                    for element in score.flat:

                        if isinstance(element, music21.note.Note):
                            if element.isRest:
                                notes.append(str(element.name))
                                durations.append(element.duration.quarterLength)
                            else:
                                notes.append(str(element.nameWithOctave))
                                durations.append(element.duration.quarterLength)

                        if isinstance(element, music21.chord.Chord):
                            notes.append(".".join(n.nameWithOctave for n in element.pitches))
                            durations.append(element.duration.quarterLength)

            with open(os.path.join(self._dirs["store"], "notes"), "wb") as f:
                pickle.dump(notes, f)
            with open(os.path.join(self._dirs["store"], "durations"), "wb") as f:
                pickle.dump(durations, f)
        else:
            with open(os.path.join(self._dirs["store"], "notes"), "rb") as f:
                notes = pickle.load(f)
            with open(os.path.join(self._dirs["store"], "durations"), "rb") as f:
                durations = pickle.load(f)

        return notes, durations

    def _lookup_tables(self):
        note_names, n_notes = unique(self._notes)
        duration_names, n_durations = unique(self._durations)
        distincts = [note_names, n_notes, duration_names, n_durations]

        with open(os.path.join(self._dirs["store"], "distincts"), "wb") as f:
            pickle.dump(distincts, f)

        note_to_int, int_to_note = lookup(note_names)
        duration_to_int, int_to_duration = lookup(duration_names)
        lookups = [note_to_int, int_to_note, duration_to_int, int_to_duration]

        with open(os.path.join(self._dirs["store"], "lookups"), "wb") as f:
            pickle.dump(lookups, f)

        return distincts, lookups

    def learn(self):
        # make the network
        network_input, network_output = sequences(self._notes, self._durations, self._lookups, self._distincts, SEQ_LEN)
        n_notes = self._distincts[1]
        n_durations = self._distincts[3]
        model, att_model = make_network(n_notes, n_durations, EMBED_SIZE, RNN_UNITS, USE_ATTENTION)

        # train the network
        checkpoint1 = ModelCheckpoint(
            os.path.join(self._dirs["weights"], "weights-improvement-{epoch:02d}-{loss:.4f}-bigger.h5"),
            monitor="loss",
            verbose=0,
            save_best_only=True,
            mode="min"
        )

        checkpoint2 = ModelCheckpoint(
            os.path.join(self._dirs['weights'], "weights.h5"),
            monitor="loss",
            verbose=0,
            save_best_only=True,
            mode="min"
        )

        early_stopping = EarlyStopping(
            monitor="loss"
            , restore_best_weights=True
            , patience = 10
        )


        callbacks_list = [
            checkpoint1
            , checkpoint2
            , early_stopping
         ]

        model.save_weights(os.path.join(self._dirs["weights"], "weights.h5"))
        history = model.fit(network_input, network_output
                  , epochs=NUM_EPOCHS
                  , batch_size=BATCH_SIZE
                  , validation_split = VAL_SPLIT
                  , callbacks=callbacks_list
                  , shuffle=SHUFFLE
                 )

        with open(os.path.join(self._dirs["store"], "history"), "wb") as f:
            pickle.dump(history.history, f)


if __name__ == "__main__":
    gakusei = Learner()
    gakusei.learn()
