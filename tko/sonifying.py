# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#  
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import functools
from . utils import *
from . composing import Composer

ddict = {
         1.0 : 'quarter',
         0.5 : 'eighth',
         0: 'zero'
        }

@default_kwargs(year=1876, seed=[["A4"],[1.0]], max_extra_notes=16, max_seq_len=32, seq_len=32, save=True)
def sonify(**kwargs):
    # load data
    df, notes_temp, duration_temp = tokyo_kion()
    idx = df.index[df["Year"]== kwargs['year']].tolist()[0]
    # params
    y = kwargs['year']
    s = kwargs['seed']
    ss = '_'.join([s[0][0], ddict[s[1][0]]])
    ntemp = notes_temp[idx]
    dtemp = duration_temp[idx]
    mxx = kwargs['max_extra_notes']
    mxl = kwargs['max_seq_len']
    sql = kwargs['seq_len']
    # load paths
    dirs = directories()
    # instantiate the Composer
    composer = Composer()
    # generate
    predictions, distribution, attention_matrix = composer.compose(
                                                                  s,
                                                                  ntemp,
                                                                  dtemp,
                                                                  mxx,
                                                                  mxl,
                                                                  sql
                                                                  )
    music_stream = predictions_to_stream(predictions)
    params_dict = {"year":y, "seed":s, "notes_temp":ntemp, "durs_temp":dtemp, "max_extra_notes":mxx, "max_seq_len":mxl, "seq_len":sql}
    if kwargs['save']:
        dirs = directories()
        # save music stream to file
        ms_filename = "tokyokionon_{}_{}_{}_{}_{}_{}_{}".format(y, ss, str(ntemp), str(dtemp), str(mxx), str(mxl), str(sql))
        stream_to_file(music_stream, dirs["mid"], outfile=ms_filename, format="midi")
        # save predictions' distribution plot       
        pd_filename = os.path.join(dirs["viz"], "predictions_{}_{}_{}_{}_{}_{}_{}".format(y, ss, str(ntemp), str(dtemp), str(mxx), str(mxl), str(sql)))
        plot_distribution(distribution, mxx, outfile=pd_filename, show=False)
        # save attention matrix plot
        am_filename = os.path.join(dirs["viz"], "attention_{}_{}_{}_{}_{}_{}_{}".format(y, ss, str(ntemp), str(dtemp), str(mxx), str(mxl), str(sql)))
        plot_attention(attention_matrix, predictions, sql, outfile=am_filename, show=False)    
    
    return music_stream, params_dict

