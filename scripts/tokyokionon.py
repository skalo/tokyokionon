# Tokyo Kion-On 
# Copyright (C) 2022  Stefano Kalonaris 
#  
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
#  
# You should have received a copy of the GNU General Public License 
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.append("../")
import json
import argparse
from tko.sonifying import sonify


parser = argparse.ArgumentParser()
parser.add_argument("-y", "--year", help="enter a year between 1876 and 2021",type=int, default=1876)
parser.add_argument("-s", "--seed", help="priming seed", type=json.loads, default=[["A4"],[1.0]])
parser.add_argument("-mxx", "--max_extra_notes", help="max extra notes", type=int, default=16)
parser.add_argument("-mxl", "--max_seq_len", help="max sequence length", type=int, default=32)
parser.add_argument("-sql", "--seq_len", help="sequence length", type=int, default=32)
args = parser.parse_args()


if __name__ == "__main__":
    sonify(**vars(args))
